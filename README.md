vodk.rs
=======

This repository contains various things I am toying with including:

- vodk_math: A strongly typed vector/matrix library (needs a real name)
- lyon: A set of tools to tesselate paths
- vodk_id: Facilities to use strongly typed ids (I use them all over the place)
- vodk_gpu: some old junk you shouldn't use because glium is better.
- ...other junk not worth looking at.

## License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
